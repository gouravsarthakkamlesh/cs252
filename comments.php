<?php
    include('connect.php');
    session_start();
    if ($_SESSION['email']=='') 
    {
    	header("Location: index.php");
    }
    elseif (isset($_GET['id'])) 
    {
    	$_SESSION['id']=$_GET['id'];
    	$post_id = $_GET['id']; 
	}
	else
	{
		header("Location: worldline.php");
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>comments</title>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<header id="header_home">
        <h2>COMMENT</h2>
    </header>
    <div id="left"> 
        <div id="left-content"> 
            <p><a href="timeline.php" id="link2">TIMELINE</a></p>
            <p><a href="worldline.php" id="link2">WORLDLINE</a></p>
        </div>
    </div>
    <div id="right"> 
        <div id="right-content">
        <br>
        <img src="logo.png" width="100" height="100"><br><br>
        <?php
            if (isset($_SESSION['email'])) {
                $now = $_SESSION['email'];
                $query = $conn -> query("SELECT * FROM user_info WHERE email = '$now'");
                $row = $query -> fetch();
        ?>
        <p id="id">
        <?php
                echo $row['firstname'].' '.$row['lastname'];
        ?>
        </p>
        <?php
            }
        ?>
            <p><a href="personal_info.php" id="link2">PERSONAL INFO</a></p>
            <p><a href="settings.php" id="link2">SETTINGS</a></p>
            <p><a href="logout.php" id="link2">LOGOUT</a></p>
        </div>
    </div>
    <div align="left" id="middle1">
    <?php
    	$com_query = $conn->query("select * from comments where post_id = $post_id");
        echo '<br><br><br><br>';
    	echo '<ul>';
        while ($com_row = $com_query->fetch()) 
        {
            $comment_by = $com_row['user_id'];
            $user_query = $conn->query("select * from user_info where id = $comment_by");
            $user_row = $user_query->fetch();
            echo '<li>' .'<span style="font-family:verdana;">'.'<span style="color:cyan;">'.'<img src="comment.png" style="width:30px;height:30px;">'. $user_row['firstname'] .' '. $user_row['lastname'] . ': ' .'</span>'.$com_row['post_comment'] .'</span>'.'</li>';
        }
        echo '</ul>';
    ?>
    <br><br><br>
    <p id="p_timeline">Comment:</p>
        <div id="textarea">
            <form action="add_comment.php" method="post" id="status_post">
                <textarea rows="10" cols="150" wrap="hard" name="comment" required="" placeholder=""></textarea><br><br>
                <button type="Submit" value="submit">SUBMIT</button> 
            </form>
        </div>
    </div>

    <footer id="footer_home">
        copyright © Gourav, Sarthak & Kamlesh
    </footer>
</body>
</html>