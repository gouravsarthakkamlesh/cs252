<!DOCTYPE html>
<html>
<head>
	<title>register</title>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<header id="header_home">
		<h2>IITK-circle Register</h2>
	</header>
	<br><br><br><br><br><br>
	<p id="p_login">Fill the following form for registering.</p>
	<p id="p_login">All * marked fields are required to be filled.</p>
	<br>
	<form id="form_login" method="post" action="add_user.php">
		First Name*:
		<input type="text" name="firstname" required><br><br>
		Last Name*:
		<input type="text" name="lastname" required><br><br>
		Date of Birth*:
		<input type="Date" name="birthday" required><br><br>
		Gender*:
		<input type="radio" name="gender" value="male"
		<?php
            if (isset($gender) && $gender == "Male") {
                echo "Male";
            }
        ?>
		>Male
		<input type="radio" name="gender" value="female"
		<?php
            if (isset($gender) && $gender == "Female") {
            	echo "Female";
            }
        ?>
		>Female
		<input type="radio" name="gender" value="other"
		<?php
            if (isset($gender) && $gender == "Other") {
            	echo "Other";
            }
        ?>
		>Other<br><br><br><br>
		IIT-K Email*:
		<input type="Email" name="email" required><br><br>
		Password*:
		<input type="Password" name="password" required><br><br><br>
		<input type="checkbox" name="accept" required> I accept all <a id="link3" href="terms_&_conditions.html">terms and conditions</a><br><br>
		<button type="submit">Register</button>
	</form><br><br><br>
	<p id="p_login">You can only register for only one account per IIT-K user id.</p>
	<footer id="footer_home">
		copyright © Gourav, Sarthak & Kamlesh 
	</footer>
</body>
</html>