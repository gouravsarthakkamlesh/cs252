<?php
    include('connect.php');
    session_start();
    if ($_SESSION['email']=='') {
        header("Location: index.php");
    }
?>
<html>
<head>
	<title>Timeline</title>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>

<body id="body_home">
	<header id="header_home">
		<h2>TIMELINE</h2>
	</header>
	<div id="left"> 
    	<div class="left-content"> 
     		<p><a href="timeline.php" id="link2">TIMELINE</a></p>
     		<p><a href="worldline.php" id="link2">WORLDLINE</a></p>
    	</div>
    </div>
    <div id="right"> 
    	<div class="right-content">
        <br>
        <img src="logo.png" width="100" height="100"><br><br>
        <?php
            if (isset($_SESSION['email'])) {
                $now = $_SESSION['email'];
                $query = $conn -> query("SELECT * FROM user_info WHERE email = '$now'");
                $user_row = $query -> fetch();
                $user_id = $user_row['id'];
        ?>
        <p id="id">
        <?php
                echo $user_row['firstname'].' '.$user_row['lastname'];
        ?>
        </p>
        <?php
            }
        ?>
    		<p><a href="personal_info.php" id="link2">PERSONAL INFO</a></p>
     		<p><a href="settings.php" id="link2">SETTINGS</a></p>
     		<p><a href="logout.php" id="link2">LOGOUT</a></p>
    	</div>
    </div>

    <div id="middle2" align="left">
        <br><br><br><br>
        <?php
            $query1 = $conn -> query("SELECT * FROM status_update WHERE user_id=$user_id");
            while ($status_row = $query1->fetch())
            {
                echo '<p id="user">' .'<img src="accept.png" style="width:50px;height:50px;">'.'<span id="p_world">'. '<a id="link3" href="comments.php?id='.$status_row['id'].'">'.$status_row['status_post'].'</a>'.'</span>';
                $post_id=$status_row['id'];
                $com_query = $conn->query("select * from comments where post_id = $post_id");
                echo '<ul>';
                while ($com_row = $com_query->fetch()) 
                {
                    $comment_by = $com_row['user_id'];
                    $user_query = $conn->query("select * from user_info where id = $comment_by");
                    $user_row = $user_query->fetch();
                    echo '<li>' .'<a href="info.php?userid='.$comment_by.'" style="color: cyan; text-decoration: none;>'.'<span style="font-family:verdana;color: white;">'.'<span style="color:cyan;">'.'<img src="comment.png" style="width:30px;height:30px;">'. $user_row['firstname'] . $user_row['lastname'] . '</a>'.': ' .'</span>'.$com_row['post_comment'] .'</span>'.'</li>';
                }
                echo '</ul>';
                echo '<br><br>';
            }
        ?>
    </div>
    
    <div id="middle1" align="centre">
        
        <p style="font-size: 20px;font-family: verdana;">What's on your Mind!!!</p>

        <form action="post.php" method="post" enctype="multipart/form-data" id="status_post">
            <textarea rows="10" cols="150" wrap="hard" name="status" required="" placeholder=""></textarea><br><br>
            <input type="submit" value="Submit">
        </form>
    </div>

	<footer id="footer_home">
		copyright © Gourav, Sarthak & Kamlesh
	</footer>
</body>
</html>