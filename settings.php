<?php
	include('connect.php');
    session_start();
    if ($_SESSION['email']=='') {
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>settings</title>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<header id="header_home">
		<h2>SETTINGS</h2>
	</header>

	<div id="leftpane">
		<button id="button"><span><a href="update_info.php" id="link4">UPDATE INFO</a></span></button>
	</div>

	<div id="rightpane">
		<button id="button"><span><a href="change_password.php" id="link4">CHANGE PASSWORD</a></span></button>
	</div>

	<footer id="footer_home">
		copyright © Gourav, Sarthak & Kamlesh
	</footer>
</body>
</html>