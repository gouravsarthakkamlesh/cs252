Sarthak Pattanayak, Gourav Katha, Kamlesh Kumar Meena

Build a social networking site. Name would be IITK-circle.


Features:
-registering users(database using sql and php). Requires linking to an iitk mail id. Also send verification mail to the mail id. Have a password as well as a security question.
-login requires username and password or answer to security question.
-only text and symbol posts.
-posts visible to all.
-store personal info of users which will be visible to all.
-only one image that would be thw profile pic.
-world line and a time line.
-time line will have your posts where as the world line will have all the posts.

Features of Database:
 A typical MySQL based database with 4 tables 
1.Comments
2.Friends
3.Status_Updates
4.User_info

Comments : Contains 4 columns i.e id,user_id , post_id and post_comments.

Friends: Contains 3 columns i.e user_id , friend_id , accepted(bool)

Status_Updates: Contains 5 columns id, user_id,status_post,title,event_time

User_info: Contains 7 columns user_id,first name,last name,email-id,password,birth,gender


