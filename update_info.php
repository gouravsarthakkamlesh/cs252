<?php
	include('connect.php');
    session_start();
    if ($_SESSION['email']=='') {
        header("Location: index.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title>update info</title>
	<link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
	<header id="header_home">
		<h2>UPDATE INFO</h2>
	</header>
	<br><br><br><br><br><br><br><br><br>
	<form id="form_login" action="update_info2.php" method="post">
		First Name:
		<input type="text" name="firstname"><br><br>
		Last Name:
		<input type="text" name="lastname"><br><br>
		Date of Birth:
		<input type="Date" name="dob"><br><br>
		Gender(mandatory to fill):
		<input type="radio" name="gender" value="male"
		<?php
            if (isset($gender) && $gender == "Male") {
                echo "Male";
            }
        ?>
		>Male
		<input type="radio" name="gender" value="female"
		<?php
            if (isset($gender) && $gender == "Female") {
            	echo "Female";
            }
        ?>
		>Female
		<input type="radio" name="gender" value="other"
		<?php
            if (isset($gender) && $gender == "Other") {
            	echo "Other";
            }
        ?>
		>Other<br><br><br>
		Password:
		<input type="Password" name="password"><br><br><br><br>
		<input type="submit" name="submit">
	</form>
	<footer id="footer_home">
		copyright © Gourav, Sarthak & Kamlesh
	</footer>
</body>
</html>