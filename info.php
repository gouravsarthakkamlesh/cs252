<?php
    include('connect.php');
    session_start();
    if ($_SESSION['email']=='') 
    {
    	header("Location: index.php");
    }
    elseif (isset($_GET['userid'])) 
    {
    	$user_id = $_GET['userid']; 
	}
	else
	{
		header("Location: worldline.php");
	}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Info</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body id="personal">
    <header id="header_home">
        <h2>User Info</h2>
    </header>
    <div id="left"> 
        <div class="left-content"> 
            <p><a href="timeline.php" id="link2">TIMELINE</a></p>
            <p><a href="worldline.php" id="link2">WORLDLINE</a></p>
        </div>
    </div>
    <div id="right"> 
        <div class="right-content">
        <br>
        <img src="logo.png" width="100" height="100"><br><br>
        <?php
            if (isset($_SESSION['email'])) {
                $now = $_SESSION['email'];
                $query = $conn -> query("SELECT * FROM user_info WHERE email = '$now'");
                $row = $query -> fetch();
        ?>
        <p id="id">
        <?php
                echo $row['firstname'].' '.$row['lastname'];
        ?>
        </p>
        <?php
            }
        ?>
            <p><a href="personal_info.php" id="link2">PERSONAL INFO</a></p>
            <p><a href="settings.php" id="link2">SETTINGS</a></p>
            <p><a href="logout.php" id="link2">LOGOUT</a></p>
        </div>
    </div>
    <?php
        $query1 = $conn -> query("SELECT * FROM user_info WHERE id = $user_id");
        $row1 = $query1 -> fetch();
    ?>
    <br><br><br><br><br>
    <h3 id="info1">Name:</h3>
    <p id="info2"><?php echo $row1['firstname'].' '.$row1['lastname']; ?></p><br>

    <h3 id="info1">Email id:</h3>
    <p id="info2"><?php echo $row1['email']; ?></p><br>
    
    <h3 id="info1">Date of Birth:</h3>
    <p id="info2"><?php echo $row1['birth']; ?></p><br>

    <h3 id="info1">Gender:</h3>
    <p id="info2"><?php echo $row1['gender']; ?></p><br>
    <footer id="footer_home">
        copyright © Gourav, Sarthak & Kamlesh
    </footer>
</body>
</html>